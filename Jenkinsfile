pipeline {
   agent none
   stages {
       stage("Build") {
           agent {
               docker {
                   image 'maven:3.6.3-jdk-8'
               }
           }
           steps {
               sh "mvn clean package -B -ntp -Dcheckstyle.skip"
           }
            post {
                always {
                    echo 'always'
                }
                success {
                    echo 'success'
                    step([
                        $class: 'JacocoPublisher',
                        execPattern: 'target/*.exec',
                        classPattern: 'target/classes',
                        sourcePattern: 'src/main/java',
                        exclusionPattern: 'src/test/*'
                    ])
                }
                failure {
                    echo 'failure'
                }
            }
       }
	   stage('Sonarqube') {
			agent {
               docker {
                   image 'maven:3.6.3-jdk-8'
               }
           }
            steps {
                withCredentials([[$class: 'FileBinding', credentialsId: 'sonarqube-settings',
                   variable: 'M2_SETTINGS']]) {
                    sh "mvn sonar:sonar -B -ntp -s ${M2_SETTINGS}"
                   }
            }
        }
		stage('Artifactory'){
			agent {
               docker {
                   image 'maven:3.6.3-jdk-8'
               }
           }
            steps{

                sh 'printenv'

                script {
                    // ====================================================
                    // Forma 1: Usando rtMaven para capturar el buildInfo
                    // ====================================================

                    // deben tener el mismo nombre que los repositrios creados en artifactory
                    def releases = 'app-war-roly-release'
                    def snapshots = 'app-war-roly-snapshot'

                    // el nombre debe ser el mismo que configuramos en jenkins
                    def server = Artifactory.server 'artifactory'
                    def rtMaven = Artifactory.newMavenBuild()

                    // setando la ruta de maven para que artifactory lo renozca a la fuerza (esto se debe a un errorcillo del plugin de artifactory)
                    env.MAVEN_HOME='/usr/share/maven'

                    rtMaven.deployer server: server, releaseRepo: releases, snapshotRepo: snapshots
                    def buildInfo = rtMaven.run pom: 'pom.xml', goals: 'clean install -B -ntp -DskipTests'
                    server.publishBuildInfo buildInfo

                    // ====================================================
                    // Forma 2: Usando FileSpecs
                    // ====================================================
                    // //usamos el la configuracion de artifactory realizada en jenkins
                    // def server = Artifactory.server 'artifactory'
                    // // es el repositorio referencia creado en artifactory
                    // def repository = 'app-coverage'

                    // // si la rama es 'master' determinamos si enviamos a release o snapshot
                    // if ("${GIT_BRANCH}" == 'master') {
                    //     repository = "${repository}-release/com/peru/app-coverage/1.0.0/"
                    // }else {
                    //     repository = "${repository}-snapshot/com/peru/app-coverage/1.0.0-SNAPSHOT-${BUILD_NUMBER}/"
                    // }

                    // def uploadSpec = """
                    // {
                    //     "files": [
                    //         {
                    //             "pattern": "target/.*.jar",
                    //             "target": "${repository}",
                    //             "regexp": "true"
                    //         }
                    //     ]
                    // }
                    // """

                    // def buildInfo = Artifactory.newBuildInfo()
                    // buildInfo.env.capture = true

                    // buildInfo.name = 'app-coverage'
                    // buildInfo.number = "${BUILD_NUMBER}"

                    // // publicando el artefacto al servidor de artifactory
                    // server.upload spec: uploadSpec, buildInfo: buildInfo
                    // server.publishBuildInfo buildInfo
                }
                
            }
        }
       stage("Approval") {
			
           steps {
               script {

                   def users = "roly.larico"

                   timeout(time: 5, unit: 'MINUTES') {
                       userInput = input(
                           submitterParameter: 'approval',
                           submitter: "${users}",
                           message: "¿Desplegar en JBoss?", parameters: [
                           [$class: 'BooleanParameterDefinition', defaultValue: true, description: '', name: 'Aprobar']
                       ])
                   }


               }
           }
       }
	   stage("Deploy on JBoss 7.2") {
           agent any
           steps {
               script {
                    sshagent ( credentials: ['jenkins-ssh-privatekey']){
                        sh """
                            scp -o StrictHostKeyChecking=no target/app-war-roly.war root@161.35.141.238:/root

                            ssh root@161.35.141.238 '~/EAP-7.2.0/bin/jboss-cli.sh -c --command="undeploy app-war-roly.war"'
                                
                            ssh root@161.35.141.238 '~/EAP-7.2.0/bin/jboss-cli.sh -c --command="deploy /root/app-war-roly.war"'

                        """
                    }
               }
           }
       }
   }
}
