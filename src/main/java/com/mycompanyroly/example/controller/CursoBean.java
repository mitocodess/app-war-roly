package com.mycompanyroly.example.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import com.mycompanyroly.example.model.Curso;
import com.mycompanyroly.example.service.CursoService;

@ManagedBean
@ViewScoped
public class CursoBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	CursoService cursoService;

	private Curso entityCurso;

	private Curso entityCursoDelete;

	private List<Curso> lstCurso = new ArrayList<Curso>();

	private FacesContext context;

	@PostConstruct
	public void init() {
		entityCurso = new Curso();
	}

	public void registrar() {
		context = FacesContext.getCurrentInstance();
		try {
			cursoService.register(entityCurso);
			entityCurso = new Curso();
			listar();
			context.addMessage(null, new FacesMessage("Curso registrado con exito", null));
		} catch (Exception e) {
			context.addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Se ha producido un error " + e.getMessage(), null));
		}
	}

	public void eliminar() {
		context = FacesContext.getCurrentInstance();
		try {
			cursoService.eliminar(entityCursoDelete);
			entityCursoDelete = new Curso();
			listar();
			context.addMessage(null, new FacesMessage("Curso eliminado con exito", null));
		} catch (Exception e) {
			e.printStackTrace();
			context.addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Se ha producido un error " + e.getMessage(), null));
		}
	}

	public void listar() throws Exception {
		lstCurso = cursoService.getlstCurso();
	}

	public Curso getEntityCurso() {
		return entityCurso;
	}

	public void setEntityCurso(Curso entityCurso) {
		this.entityCurso = entityCurso;
	}

	public List<Curso> getLstCurso() {
		return lstCurso;
	}

	public void setLstCurso(List<Curso> lstCurso) {
		this.lstCurso = lstCurso;
	}

	public Curso getEntityCursoDelete() {
		return entityCursoDelete;
	}

	public void setEntityCursoDelete(Curso entityCursoDelete) {
		this.entityCursoDelete = entityCursoDelete;
	}
}
