package com.mycompanyroly.example.service;

public class CalculadoraService {
	
	public int sumar(int a, int b) {
		return a+b;
	}
	
	public int restar(int a, int b) {
		return a-b;
	}
	
	public int multiplicar(int a, int b) {
		return a*b;
	}
	
	public Integer dividir(int a, int b) {
		try {
			return a/b;
		} catch (Exception e) {
			return null;
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
