package com.mycompanyroly.example.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.mycompanyroly.example.model.Curso;



@Stateless
public class CursoService {

	@Inject
    private EntityManager em;

	/**
	 * adicion de curso
	 * @param entity
	 * @throws Exception
	 */
	public void register(Curso entity) throws Exception {
		em.persist(entity);
		em.flush();
	}
	
	public void modificar(Curso entity) throws Exception {
		em.merge(entity);
		em.flush();
	}
	
	public void eliminar(Curso entity) throws Exception {
		em.remove(em.merge(entity));
		em.flush();
	}
	
	/**
	 * listar los cursos
	 * @return
	 * @throws Exception
	 */
	public List<Curso> getlstCurso() throws Exception{
		StringBuilder sd=new StringBuilder();
		sd.append(" select c from Curso c order by c.id desc");
		Query q=em.createQuery(sd.toString());
		return q.getResultList();
	}

}
