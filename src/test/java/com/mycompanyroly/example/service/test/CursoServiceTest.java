package com.mycompanyroly.example.service.test;

import static org.junit.Assert.assertNull;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.mycompanyroly.example.model.Curso;
import com.mycompanyroly.example.service.CursoService;

@RunWith(MockitoJUnitRunner.class)
public class CursoServiceTest {
	
	@Inject
	@Mock
	private CursoService cursoService;
	
	@Mock
	@PersistenceContext(unitName = "inscriptionDS")
	private EntityManager em;
	
//	@Test
//	public void testListaCurso()throws Exception {
//		List<Curso> lst = cursoService.getlstCurso();
//		for(Curso c:lst){
//			System.out.println("::: "+c.toString());
//		}
//	}
//	
	@Test
    public void testRegister() throws Exception {
        Curso newCurso = new Curso();
        //newCurso.setId(1L);
        newCurso.setNombreCurso("nombre curso");
        CursoService cursoServicee = Mockito.mock(CursoService.class);
        cursoServicee.register(newCurso);
        System.out.println(":::"+newCurso.getId());
        assertNull(newCurso.getId());
        //log.info(newMember.getName() + " was persisted with id " + newMember.getId());
    }
	@Test
    public void testRegisterNombreNulo() throws Exception {
        Curso newCurso = new Curso();
        //newCurso.setId(1L);
        newCurso.setNombreCurso(null);
        CursoService cursoServicee = Mockito.mock(CursoService.class);
        cursoServicee.register(newCurso);
        System.out.println(":::"+newCurso.getId());
        assertNull(newCurso.getId());
    }
}
