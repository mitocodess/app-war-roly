package com.mycompanyroly.example.service.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.mycompanyroly.example.service.CalculadoraService;

@RunWith(MockitoJUnitRunner.class)
public class CalculadoraServiceTest {
	
	@Mock
	CalculadoraService calculadoraService;
	
	public void iniciarlizar() {
		calculadoraService = new CalculadoraService();
	}
	
	@Test
	public void testSumar() {
		iniciarlizar();
		assertEquals(calculadoraService.sumar(2, 2), 4);
	}
	@Test
	public void testSumar1erNeg() {
		iniciarlizar();
		assertEquals(calculadoraService.sumar(-2, 2), 0);
	}
	@Test
	public void testSumar2doNeg() {
		iniciarlizar();
		assertEquals(calculadoraService.sumar(2, -2), 0);
	}
	@Test
	public void testSumarAmbosNeg() {
		iniciarlizar();
		assertEquals(calculadoraService.sumar(-2, -2), -4);
	}
	@Test
	public void testSumar1erMayor() {
		iniciarlizar();
		assertEquals(calculadoraService.sumar(3, 2), 5);
	}
	@Test
	public void testSumar2doMayor() {
		iniciarlizar();
		assertEquals(calculadoraService.sumar(2, 3), 5);
	}
	
	@Test
	public void restar() {
		iniciarlizar();
		assertEquals(calculadoraService.restar(5, 3), 2);
	}
	@Test
	public void restar2doMayor() {
		iniciarlizar();
		assertEquals(calculadoraService.restar(3, 5), -2);
	}
	@Test
	public void restar1erNegativo() {
		iniciarlizar();
		assertEquals(calculadoraService.restar(-3, 5), -8);
	}
	@Test
	public void restar2doNegativo() {
		iniciarlizar();
		assertEquals(calculadoraService.restar(3, -5), 8);
	}
	@Test
	public void restarAmbosNegativos() {
		iniciarlizar();
		assertEquals(calculadoraService.restar(-3, -5), 2);
	}
	@Test
	public void restarAmbosNegativosPero1roMayor() {
		iniciarlizar();
		assertEquals(calculadoraService.restar(-6, -5), -1);
	}
	@Test
	public void multiplicar() {
		iniciarlizar();
		assertEquals(calculadoraService.multiplicar(2, 2), 4);
	}
	@Test
	public void multiplicar1roCero() {
		iniciarlizar();
		assertEquals(calculadoraService.multiplicar(0, 2), 0);
	}
	@Test
	public void multiplicar2doCero() {
		iniciarlizar();
		assertEquals(calculadoraService.multiplicar(2, 0), 0);
	}
	@Test
	public void multiplicar1roNegativo() {
		iniciarlizar();
		assertEquals(calculadoraService.multiplicar(-2, 2),-4);
	}
	@Test
	public void multiplicar2doNegativo() {
		iniciarlizar();
		assertEquals(calculadoraService.multiplicar(2, -2),-4);
	}
	@Test
	public void dividir() throws Exception {
		iniciarlizar();
		assertNotNull(calculadoraService.dividir(2,2));
	}
	@Test
	public void dividir1ro0() {
		iniciarlizar();
		assertNotNull(calculadoraService.dividir(0,2));
	}
	@Test
	public void dividir2do0()  {
		iniciarlizar();
		assertNull(calculadoraService.dividir(2,0));
	}
	
//	
//	public void multiplicar() {
//		return a+b;
//	}
//	
//	public void dividir() {
//		return a+b;
//	}

}
